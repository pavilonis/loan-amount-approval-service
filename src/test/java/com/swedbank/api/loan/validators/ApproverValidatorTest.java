package com.swedbank.api.loan.validators;

import com.google.common.collect.ImmutableSet;
import com.swedbank.api.loan.LoanRequest;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

public class ApproverValidatorTest {

   private static final String CUSTOMER_ID = null;
   private final ApproverValidator testObject = new ApproverValidator();

   @Test
   public void shouldFailOnEmptyApprovers() {
      Set<String> approvers = Collections.emptySet();
      LoanRequest request = new LoanRequest(CUSTOMER_ID, BigDecimal.ZERO, approvers);
      ResponseEntity<String> result = testObject.validate(request);
      assertThat(result, notNullValue());
      assertThat(result.getBody(), containsStringIgnoringCase("approver is required"));
   }

   @Test
   public void shouldFailOnTooManyApprovers() {
      Set<String> approvers = ImmutableSet.of("Bob", "John", "James", "Robert");
      LoanRequest request = new LoanRequest(CUSTOMER_ID, BigDecimal.ZERO, approvers, LocalDateTime.now());
      ResponseEntity<String> result = testObject.validate(request);
      assertThat(result, notNullValue());
      assertThat(result.getBody(), containsStringIgnoringCase("Too many loan approvers"));
   }

   @Test
   public void shouldPass() {
      Set<String> approvers = ImmutableSet.of("Bob", "John", "James");
      LoanRequest request = new LoanRequest(CUSTOMER_ID, BigDecimal.ZERO, approvers, LocalDateTime.now());
      ResponseEntity<String> result = testObject.validate(request);
      assertThat(result, nullValue());
   }
}