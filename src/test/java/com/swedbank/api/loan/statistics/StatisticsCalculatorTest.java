package com.swedbank.api.loan.statistics;

import com.google.common.collect.ImmutableList;
import com.swedbank.api.loan.LoanRequest;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.nullValue;

public class StatisticsCalculatorTest {

   private static final long SECONDS_NOT_TESTING_YET = 777;

   @Test
   public void shouldReturnZeros() {
      Statistics result = StatisticsCalculator.calculate(Collections.emptyList(), SECONDS_NOT_TESTING_YET);
      assertThat(result.getAvg(), is(BigDecimal.ZERO));
      assertThat(result.getSum(), is(BigDecimal.ZERO));
      assertThat(result.getMin(), is(nullValue()));
      assertThat(result.getMax(), is(nullValue()));
      assertThat(result.getCount(), is(0L));
   }

   @Test
   public void simpleMathTest() {
      LocalDateTime now = LocalDateTime.now();
      List<LoanRequest> requests = ImmutableList.of(
            new LoanRequest("XXX", BigDecimal.valueOf(200L), Collections.emptySet(), now),
            new LoanRequest("YYY", BigDecimal.valueOf(300L), Collections.emptySet(), now),
            new LoanRequest("ZZZ", BigDecimal.valueOf(500L), Collections.emptySet(), now)
      );
      Statistics result = StatisticsCalculator.calculate(requests, SECONDS_NOT_TESTING_YET);
      assertThat(result.getCount(), is(equalTo((long) requests.size())));
      assertThat(result.getAvg(), is(BigDecimal.valueOf(333.33d)));
      assertThat(result.getMax(), is(BigDecimal.valueOf(500L)));
      assertThat(result.getMin(), is(BigDecimal.valueOf(200L)));
      assertThat(result.getSum(), is(BigDecimal.valueOf(1000L)));
   }
}