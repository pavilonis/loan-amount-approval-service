package com.swedbank.api.loan;

import com.swedbank.api.App;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
public class LoanRequestControllerTest {

   private static final String PAYLOAD_INCORRECT_ID = "{" +
         "  \"customerId\": \"XX-XX-YY\"," +
         "  \"amount\": 1000," +
         "  \"approvers\": [" +
         "    \"John\"" +
         "  ]" +
         "}";

   private static final String PAYLOAD_CORRECT = "{" +
         "  \"customerId\": \"12-3456-789\"," +
         "  \"amount\": 1000," +
         "  \"approvers\": [" +
         "    \"John\"" +
         "  ]" +
         "}";

   @Autowired
   private MockMvc mvc;

   @Test
   public void shouldNotPassIdValidator() throws Exception {
      mvc.perform(post("/loans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(PAYLOAD_INCORRECT_ID)
      )
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Incorrect customer ID pattern: XX-XX-YY"));
   }

   @Test
   public void shouldAcceptRequest() throws Exception {
      mvc.perform(post("/loans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(PAYLOAD_CORRECT)
      )
            .andExpect(status().isCreated());
   }

}