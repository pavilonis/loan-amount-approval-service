package com.swedbank.api.loan;

import com.swedbank.api.loan.validators.RequestValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("loans")
@RestController
public class LoanRequestController {

   private final List<RequestValidator> validators;
   private final DataStorage repository;

   public LoanRequestController(List<RequestValidator> validators, DataStorage repository) {
      this.validators = validators;
      this.repository = repository;
   }

   @PostMapping
   public ResponseEntity<?> create(@RequestBody LoanRequest request) {

      for (RequestValidator validator : validators) {
         ResponseEntity<String> errorResponse = validator.validate(request);
         if (errorResponse != null) {
            return errorResponse;
         }
      }
      return repository.storeRequest(request);
   }

   @PutMapping("approve")
   public ResponseEntity<?> approve(@RequestParam("customer-id") String customerId, @RequestParam String approver) {
      return processDecision(customerId, approver, LoanDecision.APPROVED);
   }

   @PutMapping("decline")
   public ResponseEntity<?> decline(@RequestParam String customerId, @RequestParam String approver) {
      return processDecision(customerId, approver, LoanDecision.DECLINED);
   }

   private ResponseEntity<?> processDecision(String customerId, String approver, LoanDecision decision) {
      if (!StringUtils.hasText(customerId) || !StringUtils.hasText(approver)) {
         return new ResponseEntity<>("Blank approver or customer ID", HttpStatus.BAD_REQUEST);
      }
      return repository.applyDecision(customerId, approver, decision);
   }

}
