package com.swedbank.api.loan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

@Repository
public class DataStorage {

   private static final Logger LOGGER = LoggerFactory.getLogger(DataStorage.class);
   // Could be regular HashMap, probably, because access is synchronized in method
   private final Map<String, LoanRequest> requestsPending = new ConcurrentHashMap<>();
   private final List<LoanRequest> requestsApproved = new ArrayList<>();

   ResponseEntity<?> storeRequest(LoanRequest request) {
      return synchronizedAction(() -> {
         if (requestsPending.containsKey(request.getCustomerId())) {
            return new ResponseEntity<>("Customer " + request.getCustomerId() +
                  " already has pending loan request ", HttpStatus.CONFLICT);
         }
         requestsPending.put(request.getCustomerId(), request);
         LOGGER.info("Loan added: {}", request);
         return new ResponseEntity<>(HttpStatus.CREATED);
      });
   }

   ResponseEntity<?> applyDecision(String customerId, String approver, LoanDecision decision) {
      return synchronizedAction(() -> {

         LoanRequest request = requestsPending.get(customerId);
         if (request == null) {
            return new ResponseEntity<>("Customer has no active loan request", HttpStatus.PRECONDITION_FAILED);

         } else if (!request.getApprovers().contains(approver)) {
            return new ResponseEntity<>("Supplied approver can not approve/decline this request",
                  HttpStatus.FORBIDDEN);
         }

         requestsPending.remove(customerId);

         if (decision == LoanDecision.APPROVED) {
            request.setApprovalDateTime(LocalDateTime.now());
            requestsApproved.add(request);
         }

         LOGGER.info("Loan {}: {}", decision.name(), request);
         return new ResponseEntity<>(request, HttpStatus.OK);
      });
   }

   /**
    * Using this method to avoid race conditions during data manipulation
    */
   private synchronized ResponseEntity<?> synchronizedAction(Supplier<ResponseEntity<?>> action) {
      return action.get();
   }

   public List<LoanRequest> approved() {
      return requestsApproved;
   }
}
