package com.swedbank.api.loan.validators;

import com.swedbank.api.loan.LoanRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;
import java.util.regex.Pattern;

@Component
public class CustomerIdValidator implements RequestValidator {

   private static final String CUSTOMER_ID_PATTERN = "^[A-Za-z0-9]{2}-[A-Za-z0-9]{4}-[A-Za-z0-9]{3}$";
   private final Predicate<String> customerIdValidator = Pattern.compile(CUSTOMER_ID_PATTERN)
         .asPredicate();

   @Override
   public ResponseEntity<String> validate(LoanRequest request) {
      if (customerIdValidator.test(request.getCustomerId())) {
         return null;
      }
      return new ResponseEntity<>("Incorrect customer ID pattern: " + request.getCustomerId(),
            HttpStatus.BAD_REQUEST);
   }
}
