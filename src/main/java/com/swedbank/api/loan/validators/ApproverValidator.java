package com.swedbank.api.loan.validators;

import com.swedbank.api.loan.LoanRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ApproverValidator implements RequestValidator {

   private static final int APPROVERS_LIMIT = 3;

   @Override
   public ResponseEntity<String> validate(LoanRequest request) {

      if (request.getApprovers().isEmpty()) {
         return new ResponseEntity<>("At least 1 loan approver is required", HttpStatus.BAD_REQUEST);

      } else if (request.getApprovers().size() > APPROVERS_LIMIT) {
         return new ResponseEntity<>("Too many loan approvers. Max " +
               APPROVERS_LIMIT + " approvers are allowed", HttpStatus.BAD_REQUEST);

      } else if (!request.getApprovers().stream().allMatch(StringUtils::hasText)) {
         return new ResponseEntity<>("Blank approver usernames are not expected", HttpStatus.BAD_REQUEST);
      }

      return null;
   }
}
