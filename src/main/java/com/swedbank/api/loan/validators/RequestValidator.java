package com.swedbank.api.loan.validators;

import com.swedbank.api.loan.LoanRequest;
import org.springframework.http.ResponseEntity;

public interface RequestValidator {
   /**
    * @return response entity with text description body and 4XX HTTP response
    * if validation failed, NULL if validation passed
    */
   ResponseEntity<String> validate(LoanRequest request);
}
