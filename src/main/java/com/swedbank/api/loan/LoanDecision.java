package com.swedbank.api.loan;

public enum LoanDecision {
   APPROVED, DECLINED
}
