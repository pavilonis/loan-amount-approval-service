package com.swedbank.api.loan;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

public final class LoanRequest {

   private final String customerId;
   private final BigDecimal amount;
   private final Set<String> approvers;

   @JsonIgnore
   private LocalDateTime approvalDateTime;

   public LoanRequest(@JsonProperty("customerId") String customerId,
                      @JsonProperty("amount") BigDecimal amount,
                      @JsonProperty("approvers") Set<String> approvers) {

      this(customerId, amount, approvers, null);
   }

   public LoanRequest(String customerId, BigDecimal amount, Set<String> approvers, LocalDateTime approvalDateTime) {
      this.customerId = customerId;
      this.amount = amount;
      this.approvers = approvers;
      this.approvalDateTime = approvalDateTime;
   }

   public String getCustomerId() {
      return customerId;
   }

   public BigDecimal getAmount() {
      return amount;
   }

   public Set<String> getApprovers() {
      return approvers;
   }

   public LocalDateTime getApprovalDateTime() {
      return approvalDateTime;
   }

   public void setApprovalDateTime(LocalDateTime approvalDateTime) {
      this.approvalDateTime = approvalDateTime;
   }

   @Override
   public String toString() {
      return "customerId='" + customerId + '\'' + ", amount=" + amount + ", approvers=" + approvers;
   }
}
