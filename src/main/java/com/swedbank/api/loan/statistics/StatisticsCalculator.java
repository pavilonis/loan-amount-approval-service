package com.swedbank.api.loan.statistics;

import com.swedbank.api.loan.LoanRequest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Collection;

final class StatisticsCalculator {

   private StatisticsCalculator() {/**/}

   static Statistics calculate(Collection<LoanRequest> requests, long secondsAgo) {

      long count = 0;
      BigDecimal sum = BigDecimal.ZERO;
      BigDecimal max = null;
      BigDecimal min = null;

      LocalDateTime periodThreshold = LocalDateTime.now()
            .minusSeconds(secondsAgo);

      for (LoanRequest request : requests) {
         if (request.getApprovalDateTime().isBefore(periodThreshold)) {
            continue;
         }
         count++;
         BigDecimal amount = request.getAmount();
         sum = sum.add(amount);
         if (max == null || max.compareTo(amount) < 0) {
            max = amount;
         }
         if (min == null || min.compareTo(amount) > 0) {
            min = amount;
         }
      }

      BigDecimal avg = count == 0
            ? BigDecimal.ZERO
            : sum.divide(BigDecimal.valueOf(count), 2, RoundingMode.HALF_UP);

      return new Statistics(count, sum, avg, max, min);
   }
}
