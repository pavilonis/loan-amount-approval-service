package com.swedbank.api.loan.statistics;

import java.math.BigDecimal;

final class Statistics {

   private final long count;
   private final BigDecimal sum;
   private final BigDecimal avg;
   private final BigDecimal max;
   private final BigDecimal min;

   Statistics(long count, BigDecimal sum, BigDecimal avg, BigDecimal max, BigDecimal min) {
      this.count = count;
      this.sum = sum;
      this.avg = avg;
      this.max = max;
      this.min = min;
   }

   public long getCount() {
      return count;
   }

   public BigDecimal getSum() {
      return sum;
   }

   public BigDecimal getAvg() {
      return avg;
   }

   public BigDecimal getMax() {
      return max;
   }

   public BigDecimal getMin() {
      return min;
   }
}

