package com.swedbank.api.loan.statistics;

import com.swedbank.api.loan.DataStorage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("statistics")
@RestController
public class StatisticsController {

   private static final long STATISTICS_PERIOD_SECONDS = 60;
   private final DataStorage repository;

   public StatisticsController(DataStorage repository) {
      this.repository = repository;
   }

   @GetMapping
   public ResponseEntity<Statistics> statistics() {
      Statistics stats = StatisticsCalculator.calculate(repository.approved(), STATISTICS_PERIOD_SECONDS);
      return new ResponseEntity<>(stats, HttpStatus.OK);
   }

}
