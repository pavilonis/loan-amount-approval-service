
package com.swedbank.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.ServerResponse;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.URI;

import static org.springframework.web.servlet.function.RequestPredicates.GET;
import static org.springframework.web.servlet.function.RouterFunctions.route;

@EnableSwagger2
@SpringBootApplication
public class App {

   public static void main(String[] args) {
      SpringApplication.run(App.class, args);
   }

   @Bean
   public RouterFunction<ServerResponse> routerFunction() {
      return route(GET("/"), req -> ServerResponse.temporaryRedirect(URI.create("swagger-ui.html")).build());
   }

   @Bean
   public Docket rest() {
      return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.swedbank.api"))
            .paths(PathSelectors.regex("/.*"))
            .build()
            .apiInfo(restInfo());
   }

   private ApiInfo restInfo() {
      return new ApiInfoBuilder()
            .title("Loan Amount approval service")
            .description("DESCRIPTION")
            .version("VERSION")
            .termsOfServiceUrl("http://swedbank.lt")
            .license("LICENSE")
            .licenseUrl("http://swedbank.lt")
            .build();
   }
}
