FROM java:8-jdk-alpine
COPY ./build/libs/loan-approval-service.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "loan-approval-service.jar"]