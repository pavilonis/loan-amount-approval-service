**Loan amount approval service**

The easiest way to run the application is to use docker command:
```
docker run -p 8080:8080 -it pavilonis/loan-approval-service
```
Application will process requests sent to 8080 port.
To interact with application you can use included a web based **Swagger UI** 
which is accessible from application's root url path ``http://localhost:8080/``
